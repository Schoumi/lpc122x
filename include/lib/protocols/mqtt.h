/****************************************************************************
 *   lib/protocols/mqtt/mqtt.h
 *
 * Copyright 2019 Nathael Pajani <nathael.pajani@ed3l.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *************************************************************************** */

/* 
 * MQTT client implementation for embeded microcontrollers.
 *
 * For protocol defiition, refer to 
 * http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html
 *
 * This code is the implementation of the MQTT protocol part of the communication.
 * The MQTT protocol requires a lossless, ordered transport protocol layer with an address mechanism
 * which is not part of the code found in mqtt.c and mqtt.h, making it possible to use any underlying
 * communication layer which fulfills the above requirements.
 *
 * This code only handles the packet encoding and decoding according to the MQTT specification and
 * provides usage information in order to respect the protocol flow, but does not provide the communication
 * parts of the protocol, which should be application specific.
 * This lets the application designer choose between any of the possible MQTT flow available, from single
 * "in-flight" message window to a single server to multiple servers and complex message queues.
 * 
 */

#ifndef MQTT_H
#define MQTT_H

#include "core/system.h"

/***************************************************************************** */


/* Protocol identifiers for MQTT v3.1.1. */
#define MQTT_PROTOCOL_LEVEL 0x04
#define MQTT_PROTOCOL_NAME   "MQTT"


/* MQTT control packet types. */
enum MQTT_message_types {
	MQTT_CONTROL_CONNECT = 1,
	MQTT_CONTROL_CONNACK,
	MQTT_CONTROL_PUBLISH,
	MQTT_CONTROL_PUBACK,
	MQTT_CONTROL_PUBREC,
	MQTT_CONTROL_PUBREL,
	MQTT_CONTROL_PUBCOMP,
	MQTT_CONTROL_SUBSCRIBE,
	MQTT_CONTROL_SUBACK,
	MQTT_CONTROL_UNSUBSCRIBE,
	MQTT_CONTROL_UNSUBACK,
	MQTT_CONTROL_PINGREQ,
	MQTT_CONTROL_PINGRESP,
	MQTT_CONTROL_DISCONNECT
};

/* MQTT QoS levels */
#define MQTT_QoS_0  (0)
#define MQTT_QoS_1  (1)
#define MQTT_QoS_2  (2)


struct mqtt_topic {
	char* name;
	uint8_t QoS; /* 0, 1 or 2 */
};


/***************************************************************************** */
/* Connect and Connack packets */

/* CONNECT packet flags */
#define MQTT_CONNECT_FLAG_CLEAN_SESSION (0x01 << 1)
#define MQTT_CONNECT_FLAG_WILL_FLAG  (0x01 << 2)
#define MQTT_CONNECT_FLAG_WILL_QoS(x)  (((x) & 0x03) << 3)
#define MQTT_CONNECT_FLAG_WILL_RETAIN  (0x01 << 5)
#define MQTT_CONNECT_FLAG_PASSWORD  (0x01 << 6)
#define MQTT_CONNECT_FLAG_USER_NAME  (0x01 << 7)

struct mqtt_connect_pkt_fixed_payload {
	uint16_t proto_name_len; /* network endian */
	uint8_t protocol_name[4]; /* "MQTT" : use MQTT_PROTOCOL_NAME */
	uint8_t protocol_level;  /* use MQTT_PROTOCOL_LEVEL */
	uint8_t connect_flags;
	uint16_t keep_alive_seconds; /* network endian */
};

#define MQTT_SESSION_RESUME 0x00
#define MQTT_SESSION_NEW    0x01
struct mqtt_connect_pkt {
	uint16_t keep_alive_seconds;
	uint8_t clean_session_flag; /* Either MQTT_SESSION_RESUME or MQTT_SESSION_NEW */
	char* client_id;
	struct mqtt_topic will_topic;
	uint8_t will_retain; /* 0 or 1 */
	uint16_t will_msg_size; /* stored in host endianness */
	uint8_t* will_msg;
	char* username;
	char* password;
};

/* Create MQTT connect packet
 * This function must be called in order to create the connect MQTT packet used to connect to the
 * server.
 * The client must send a connect packet whenever the server does not acknoledge a publish,
 * subscribe, unsubscribe, or ping packet, or when the server explicitely closes the connection.
 *
 * Caller must provide a buffer "buf" big enougth to receive the whole packet and indicate it's size
 * in "buf_size".
 * Returns the used buffer size (packet size) on success.
 * Return value is negative on error :
 *   -EINVAL if either buf or pkt is NULL,
 *   -EINVAL if client_id string provided in mqtt_connect_pkt struct is NULL (it's length may be 0,
 *     but pointer cannot bu NULL).
 *   -E2BIG if buffer is not big enough for packet.
 */
int mqtt_pack_connect_packet(const struct mqtt_connect_pkt* pkt, uint8_t* buf, uint32_t buf_size);


/* Connack return codes returned in a CONNACK packet */
enum MQTT_connack_return_codes {
	MQTT_CONNACK_ACCEPTED = 0,
	MQTT_CONNACK_REFUSED_PROTOCOL_VERSION = 1,
	MQTT_CONNACK_REFUSED_IDENTIFIER_REJECTED = 2,
	MQTT_CONNACK_REFUSED_SERVER_UNAVAILABLE = 3,
	MQTT_CONNACK_REFUSED_BAD_USER_NAME_OR_PASSWORD = 4,
	MQTT_CONNACK_REFUSED_NOT_AUTHORIZED = 5,
	MQTT_CONNACK_MAX,
};

/* MQTT connack packet */
struct mqtt_connack_reply_pkt {
	uint8_t control; /* Paquet type : must be (MQTT_CONTROL_CONNACK << 4) */
	uint8_t rem_len; /* Remaining length : must be 0x02 */
	uint8_t flags;  /* Connect acknowledge flags */
	uint8_t ret_code; /* Connect return code */
} __attribute__ ((__packed__));

/* Connect acknowledge flags bit 0 set to 1 only if connect with Clean session flag was set to 0 and
 * the server accepts the connection and has a stored session for this client id */
#define MQTT_CONNACK_SESSION_PRESENT  (0x01 << 0)

/* Check MQTT connack packet
 * This function may get called to check a supposed connect acknowledge packet.
 * The function checks for conformance to the MQTT protocol and returns 0 if the packet is valid and
 * the server accepted the connection.
 * The function returns -EINVAL if pkt is NULL, -EPROTO in case of invalid connack packet or
 *  -EREMOTEIO in case of connection refused by the server (ret_code is then valid in the packet).
 */
int mqtt_check_connack_reply_pkt(const struct mqtt_connack_reply_pkt* pkt);

/***************************************************************************** */
/* publish and puback packets */

#define MQTT_PUBLISH_DUP  (0x01 << 3)
#define MQTT_PUBLISH_QoS(x)  (((x) & 0x03) << 1)
#define MQTT_PUBLISH_RETAIN  (0x01 << 0)

/* MQTT publish paquet
 * A publish control packet is sent from a Client to a Server or from Server to a Client to transport
 * an Application Message. */
struct mqtt_publish_pkt {
	struct mqtt_topic topic;
	uint16_t packet_id; /* Packet identifier is required for publish if QoS > 0 */
	uint8_t dup_flag; /* Accept 1 or MQTT_PUBLISH_DUP as flag set */
	uint8_t retain_flag;
	uint32_t message_size;
	uint8_t* application_message;
};

/* Create MQTT publish packet
 * This function must be called in order to create a publish MQTT packet used to publish data on a
 * topic (send data to the server).
 * The application message size can be 0, in which case the application_message pointer in the
 * mqtt_publish_pkt struct may be NULL
 *
 * Caller must provide a buffer "buf" big enougth to receive the whole packet and indicate it's size
 * in "buf_size".
 * Returns the used buffer size (packet size) on success.
 * Return value is negative on error :
 *   -EINVAL if either buf or pkt is NULL,
 *   -EINVAL if client_id string provided in mqtt_connect_pkt struct is NULL (it's length may be 0,
 *     but pointer cannot bu NULL).
 *   -E2BIG if buffer is not big enough for packet.
 */
int mqtt_pack_publish_packet(const struct mqtt_publish_pkt* pkt, uint8_t* buf, uint32_t buf_size);


/* Unpack MQTT publish packet
 * This function must be called in order to transform a received publish MQTT packet to a
 * mqtt_publish_pkt structure.
 * The function also checks the validity of the packet.
 * All returned pointers within the struct will point to parts of the provided buffer, so the buffer
 * must not be discarded after the call.
 * if the return value is positive, it is the topic string length.
 */
int mqtt_unpack_publish_packet(struct mqtt_publish_pkt* pkt, uint8_t* buf, uint32_t size);


#define MQTT_PUBREL_FLAG  (0x01 << 1)

/* MQTT publish replies packet, used for puback, pubrec, pubrel and pubcomp */
/* control paquet type must be either (MQTT_CONTROL_PUBACK << 4) if QoS = 1 (no further reply required)
 * or (MQTT_CONTROL_PUBREC << 4) if QoS = 2 and then a publish release must be received or sent
 * (MQTT_CONTROL_PUBREL << 4), answered by a publish complete packet (MQTT_CONTROL_PUBCOMP << 4) */
struct mqtt_publish_reply_pkt {
	uint8_t control; /* Packet type */
	uint8_t rem_len; /* Remaining length : must be 0x02 */
	uint16_t acked_pkt_id; /* Id of packet that is being acknowledged, in network endianness */
} __attribute__ ((__packed__));

/* Build MQTT puback, pubrec, pubrel or pubcomp packet, used in the publish acknowledge one-way or
 * two-way hand-check mechanism.
 * The provided buf must be big enough to hold 4 bytes.
 * Note that buf may safely be cast to or from a mqtt_publish_reply_pkt struct pointer.
 * type is one of MQTT_CONTROL_PUBACK, MQTT_CONTROL_PUBREC, MQTT_CONTROL_PUBREL or MQTT_CONTROL_PUBCOMP.
 * acked_pkt_id is the ID of the concerned publish packet, in host endianness.
 * Returns the used buffer size (4 bytes) on success, or -EINVAL in case of a NULL buf pointer.
 */
int mqtt_pack_publish_reply_pkt(uint8_t* buf, uint16_t acked_pkt_id, uint8_t type);

/* Check MQTT puback, pubrec, pubrel or pubcomp packet
 * This function may get called to check a supposed publish acknowledge packet in either one-way or
 * two-way hand-check mechanism.
 * type is either MQTT_CONTROL_PUBACK, MQTT_CONTROL_PUBREC, MQTT_CONTROL_PUBREL or MQTT_CONTROL_PUBCOMP.
 * The function checks for conformance to the MQTT protocol and returns 0 if the packet is valid.
 * The function returns -EPROTO in case of protocol error or -EINVAL in case of a NULL pkt pointer.
 */
int mqtt_check_publish_reply_pkt(struct mqtt_publish_reply_pkt* pkt, uint8_t type);



/***************************************************************************** */
/* subsribe, unsubsribe, suback and unsuback packets */

#define MQTT_SUBSCRIBE_FLAG  (0x01 << 1)
#define MQTT_UNSUBSCRIBE_FLAG  MQTT_SUBSCRIBE_FLAG

/* MQTT subscribe or unsubsribe packet */
struct mqtt_sub_pkt {
	uint16_t packet_id; /* Packet identifier */
	uint8_t nb_topics; /* Number of topics in the topics table. Limited to 125 */
	struct mqtt_topic* topics; /* Table of topics */
};

/* Build MQTT subscribe packet
 * This function must be called in order to create a subscribe MQTT packet used to
 * subscibe on a topic (or multiple topics) in order to receive data published on this
 * or these topics.
 * We limit the number of subscriptions sent at once to 125 in order to get a fixed size
 * subscription acknoledgement packet.
 */
int mqtt_pack_subscribe_pkt(const struct mqtt_sub_pkt* pkt, uint8_t* buf, uint32_t buf_size);

/* Build MQTT unsubscribe packet
 * This function must be called in order to create an unsubscribe MQTT packet used to unsubscibe
 * from a topic (or multiple topics) in order to stop receiving data published on this or these
 * topics.
 */
int mqtt_pack_unsubscribe_pkt(const struct mqtt_sub_pkt* pkt, uint8_t* buf, uint32_t buf_size);



/* MQTT subscribe or unsubscribe reply packet */
struct mqtt_sub_reply_pkt {
	uint8_t control; /* Packet type */
	uint8_t rem_len; /* Remaining length : this is valid for up to 125 subscriptions sent at once ... */
	uint16_t acked_pkt_id; /* Id of packet that is being acknowledged, in network endianness */
} __attribute__ ((__packed__));

#define MQTT_SUBSCRIBE_FAILURE   (0x80)

/* Check MQTT suback packet
 * This function may get called to check a supposed subscribe acknowledge packet.
 * The function checks for conformance to the MQTT protocol and returns 0 if the packet is valid,
 * regardless of the return codes received.
 * len must be the length of the full packet received, which includes the mqtt_subscribe_reply_pkt
 * structure and all the return codes received.
 */
int mqtt_check_suback_reply_pkt(const struct mqtt_sub_reply_pkt* pkt, uint8_t len);

/* Check MQTT unsuback packet
 * This function may get called to check a supposed unsubscribe acknowledge packet.
 * The function checks for conformance to the MQTT protocol and returns 0 if the packet is valid.
 */
int mqtt_check_unsuback_reply_pkt(const struct mqtt_sub_reply_pkt* pkt);




/***************************************************************************** */
/* MQTT ping request and reply packet */
struct mqtt_ping_pkt {
	uint8_t control; /* Packet type : either (MQTT_CONTROL_PINGREQ << 4) or (MQTT_CONTROL_PINGRESP << 4) */
	uint8_t rem_len; /* Remaining length : must be 0x00 */
} __attribute__ ((__packed__));

/* Build MQTT ping packet
 * This one is a fixed packet, easy.
 */
int mqtt_pack_ping_pkt(uint8_t* buf);

/* Check MQTT ping reply packet */
int mqtt_check_ping_reply_pkt(const struct mqtt_ping_pkt* pkt);



/***************************************************************************** */
/* MQTT disconnect */
/* This one may not be very useful in most applications, but it's defined by the MQTT standard and
 * including the definition here takes no room in either code or data ...
 */
struct mqtt_disconnect_pkt {
	uint8_t control; /* Packet type : (MQTT_CONTROL_DISCONNECT << 4) */
	uint8_t rem_len; /* Remaining length : must be 0x00 */
} __attribute__ ((__packed__));



#endif /* MQTT_H */

