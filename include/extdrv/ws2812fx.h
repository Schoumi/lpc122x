/*
  WS2812FX.h - Library for WS2812 LED effects.

  Harm Aldick - 2016
  www.aldick.org
  FEATURES
/    * A lot of blinken modes and countin2
    * WS2812FX can be used as drop-in replacement for Adafruit Neopixel Library
  NOTES
    * Uses the Adafruit Neopixel library. Get it here:
      https://github.com/adafruit/Adafruit_NeoPixel
  LICENSE
  The MIT License (MIT)
  Copyright (c) 2016  Harm Aldick
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  CHANGELOG
  2016-05-28   Initial beta release
  2016-06-03   Code cleanup, minor improvements, new modes
  2016-06-04   2 new fx, fixed setColor (now also resets _mode_color)
  2017-02-02   added external trigger functionality (e.g. for sound-to-light)
*/

#ifndef WS2812FX_h
#define WS2812FX_h

#include "core/pio.h"
#include "extdrv/ws2812.h"

#define FX_DEFAULT_BRIGHTNESS 50
#define FX_DEFAULT_MODE 0
#define FX_DEFAULT_SPEED 150
#define FX_DEFAULT_COLOR 0xFF0000

#define FX_SPEED_MIN 0
#define FX_SPEED_MAX 255

#define FX_BRIGHTNESS_MIN 0
#define FX_BRIGHTNESS_MAX 255

enum ModeFX {
	FX_MODE_STATIC,
	FX_MODE_BLINK,
	FX_MODE_BREATH,
	FX_MODE_COLOR_WIPE,
	FX_MODE_COLOR_WIPE_RANDOM,
	FX_MODE_RANDOM_COLOR,
	FX_MODE_SINGLE_DYNAMIC,
	FX_MODE_MULTI_DYNAMIC,
	FX_MODE_RAINBOW,
	FX_MODE_RAINBOW_CYCLE,
	FX_MODE_THEATER_CHASE,
	FX_MODE_THEATER_CHASE_RAINBOW,
	FX_MODE_SCAN,
	FX_MODE_DUAL_SCAN,
	FX_MODE_FADE,
#if 0
	FX_MODE_RUNNING_LIGHTS,
#endif
	FX_MODE_TWINKLE,
	FX_MODE_TWINKLE_RANDOM,
	FX_MODE_TWINKLE_FADE,
	FX_MODE_TWINKLE_FADE_RANDOM,
	FX_MODE_SPARKLE,
	FX_MODE_FLASH_SPARKLE,
	FX_MODE_HYPER_SPARKLE,
	FX_MODE_STROBE,
	FX_MODE_STROBE_RAINBOW,
	FX_MODE_MULTI_STROBE,
	FX_MODE_BLINK_RAINBOW,
	FX_MODE_CHASE_WHITE,
	FX_MODE_CHASE_COLOR,
	FX_MODE_CHASE_RANDOM,
	FX_MODE_CHASE_RAINBOW,
	FX_MODE_CHASE_FLASH,
	FX_MODE_CHASE_FLASH_RANDOM,
	FX_MODE_CHASE_RAINBOW_WHITE,
	FX_MODE_CHASE_BLACKOUT,
	FX_MODE_CHASE_BLACKOUT_RAINBOW,
	FX_MODE_COLOR_SWEEP_RANDOM,
	FX_MODE_RUNNING_COLOR,
	FX_MODE_RUNNING_RED_BLUE,
	FX_MODE_RUNNING_RANDOM,
	FX_MODE_LARSON_SCANNER,
	FX_MODE_COMET,
	FX_MODE_FIREWORKS,
	FX_MODE_FIREWORKS_RANDOM,
	FX_MODE_MERRY_CHRISTMAS,
	FX_MODE_FIRE_FLICKER,
	FX_MODE_FIRE_FLICKER_SOFT,
	FX_MODE_COUNT
};

struct ws2812fx {
	struct ws2812_conf* wsconf;
	int _running;
	int _triggered;

	uint32_t _color;
	uint32_t _counter_mode_call;
	uint32_t _counter_mode_step;
	uint32_t _mode_color;
	uint32_t _mode_delay;

	unsigned long _mode_last_call_time;

	uint16_t _led_count;

	uint8_t _mode_index;
	uint8_t _speed;
	uint8_t _brightness;
};

void WS2812FX(struct ws2812fx* fx, uint16_t n, const struct pio* pin);

void FX_init(struct ws2812fx* fx);
void FX_service(struct ws2812fx* fx);
void FX_start(struct ws2812fx* fx);
void FX_stop(struct ws2812fx* fx);
void FX_setMode(struct ws2812fx* fx, uint8_t m);
void FX_setSpeed(struct ws2812fx* fx, uint8_t s);
void FX_increaseSpeed(struct ws2812fx* fx, uint8_t s);
void FX_decreaseSpeed(struct ws2812fx* fx, uint8_t s);
void FX_setRGBColor(struct ws2812fx* fx, uint8_t r, uint8_t g, uint8_t b);
void FX_setColor(struct ws2812fx* fx, uint32_t c);
void FX_trigger(struct ws2812fx* fx);
void FX_setBrightness(struct ws2812fx* fx, uint8_t b);
void FX_increaseBrightness(struct ws2812fx* fx, uint8_t s);
void FX_decreaseBrightness(struct ws2812fx* fx, uint8_t s);

int FX_isRunning(struct ws2812fx* fx);

uint8_t FX_getMode(struct ws2812fx* fx);
uint8_t FX_getSpeed(struct ws2812fx* fx);
uint8_t FX_getBrightness(struct ws2812fx* fx);
uint8_t FX_getModeCount(struct ws2812fx* fx);

uint32_t FX_getColor(struct ws2812fx* fx);

const char* FX_getModeName(uint8_t m);

#endif
