/****************************************************************************
 *   extdrv/bq769x0_bms.h
 *
 *
 * Copyright 2020 Nathael Pajani <nathael.pajani@ed3l.fr>
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *************************************************************************** */

#ifndef EXTDRV_BQ769X0_H
#define EXTDRV_BQ769X0_H

#include "lib/stdint.h"


/***************************************************************************** */
/*          Support for BQ769X0 Battery Protection from Texas Instrument       */
/***************************************************************************** */
/* This driver is made for the BQ76920 version of the chip, though there's few
 * diferences between the BQ76920, BQ76930 and BQ76950 versions.
 * This driver does not handle the Alert command.
 */

/* BQ769X0 instance data.
 * Use one of this for each BMS you want to access.
 * - addr is the BMS address on most significant bits.
 */
struct bq769x0_bms_conf {
	uint8_t addr;
	uint8_t bus_num;
	uint8_t probe_ok;
	uint8_t crc_check; /* 0 : No CRC check */
	uint8_t adc_gain;
	int8_t adc_offset;
	uint16_t adc_gain_uV; /* ADC gain converted to µV */
};

/* Check the bms presence, return 1 if found
 * This is a basic check, it could be anything with the same address ...
 */
int bq769x0_probe_bms(struct bq769x0_bms_conf* conf);


/* Read the BMS status Word and erase it's content
 * conf: the bms configuration structure.
 * status: pointer to an uint8_t value to store the old status value. May NOT be NULL.
 * Return value:
 *   Upon successfull completion, returns 0. On error, returns a negative integer
 *   equivalent to errors from glibc.
 */
int bq769x0_bms_get_and_erase_status(struct bq769x0_bms_conf* conf, uint8_t* status);

int bq769x0_bms_set_regs(struct bq769x0_bms_conf* conf,
				uint8_t reg_start, uint8_t* values, uint8_t len);


/* BMS config
 * Performs default configuration of the BMS.
 * conf: the bms configuration structure.
 * old_status: pointer to an uint8_t value to store the old status value. May be NULL.
 * Return value:
 *   Upon successfull completion, returns 0. On error, returns a negative integer
 *   equivalent to errors from glibc.
 */
int bq769x0_bms_config(struct bq769x0_bms_conf* conf, uint8_t* old_status);

#endif /* EXTDRV_BQ769X0_H */

