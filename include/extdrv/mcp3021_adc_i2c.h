/****************************************************************************
 *   extdrv/mcp3021_adc_i2c.h
 *
 *
 * Copyright 2021 Nathael Pajani <nathael.pajani@ed3l.fr>
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *************************************************************************** */

#ifndef MCP3021_ADC_H
#define MCP3021_ADC_H


/***************************************************************************** */
/*          Support for MCP3021 I2C ADC from Microchip                         */
/***************************************************************************** */

/* The MCP3021 has no internal registers.
 * Data sampling is triggered by reading from the device
 *  (triggering occurs on the R/W bit of the address byte set to Read)
 * Continuous sampling is triggered by continuous reading.
 * Samples are received on two contiguous bytes, with the four upper bits on the
 *   four lower bits of the first byte and the six lower bits on the six upper
 *   bits of the second byte.
 */

struct mcp3021_config {
	uint8_t bus_num;
	uint8_t addr;
	uint8_t probe_ok;
};

/* ADC Read
 * Read _len_ samples from the ADC, put the shifted samples into _values_
 * Return value(s):
 *   Upon successfull completion, returns the number of samples read.
 *   On error, returns a negative integer equivalent to errors from glibc.
 */
int mcp3021_read(struct mcp3021_config* conf, uint16_t* values, int len);

int mcp3021_config(struct mcp3021_config* conf);


#endif /* MCP3021_ADC_H */

