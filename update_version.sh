#! /bin/sh

# This script must be run from the application directory which wants to use it

# Check header file and version counter files presence
if [ ! -f .version ] ; then
	echo "No initial version counter, setting to version 1"
	echo 1 > .version
fi
if [ ! -f version.h ] ; then
	echo "Need version.h file"
	exit 3
fi

# Read and increment version number
i=$(cat .version)
i=$((i + 1))
echo $i > .version

#update version number in header file
sed -i "s/COMPILE_VERSION.*/COMPILE_VERSION \"$i\"/" version.h
