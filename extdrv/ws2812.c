/****************************************************************************
 *   extdrv/ws2812.c
 *
 *
 * Copyright 2013 Nathael Pajani <nathael.pajani@ed3l.fr>
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *************************************************************************** */

/*
 * Support for the WS2812 and WS2812B Chainable RGB Leds
 *
 * WS2812 protocol can be found here : https://www.adafruit.com/datasheets/WS2812.pdf
 *
 */

#include "core/system.h"
#include "core/pio.h"
#include "drivers/gpio.h"
#include "lib/string.h"
#include "extdrv/ws2812.h"



/* Configure Selected GPIO as output. */
void ws2812_config(struct ws2812_conf* ws2812, const struct pio* gpio)
{
	config_pio(gpio, (LPC_IO_MODE_PULL_UP | LPC_IO_DIGITAL));
	pio_copy(&(ws2812->gpio), gpio);
	gpio_dir_out(ws2812->gpio);

	ws2812->brightness = 256;
	ws2812->nb_bytes = 0;
	ws2812->max_led = 0;

	if (ws2812->inverted == 0) {
		gpio_clear(ws2812->gpio);
	} else {
		gpio_set(ws2812->gpio);
	}
}


static void ws2812_bit_sender(struct ws2812_conf* ws2812)
{
	struct lpc_gpio* gpio_port = LPC_GPIO_REGS(ws2812->gpio.port);
	uint32_t gpio_bit = (1 << ws2812->gpio.pin);
	uint32_t byte_idx = 0;
	uint8_t bit_idx = 7;
	uint8_t bit = 0;
	uint8_t data = (ws2812->led_data[0] * ws2812->brightness) >> 8;

	lpc_disable_irq();

	/* Send data */
	while (byte_idx < ws2812->nb_bytes) {
		bit = (data & (0x01 << bit_idx));

		if (bit == 0) {
			/* "high" time : 350ns */
			gpio_port->toggle = gpio_bit;
			nop();
			nop();
			nop();
			nop();
			nop();
			nop();
			/* "low" time : 800ns */
			gpio_port->toggle = gpio_bit;
			nop();
			nop();
			nop();
			nop();
			nop();
			nop();
			nop();
		} else {
			/* "high" time : 800ns */
			gpio_port->toggle = gpio_bit;
			nop();
			nop();
			nop();
			nop();
			nop();
			nop();
			nop();
			nop();
			nop();
			nop();
			nop();
			nop();
			nop();
			nop();
			nop();
			nop();
			nop();
			nop();
			/* "low" time : 400ns */
			gpio_port->toggle = gpio_bit;
		}

		/* Move to the next bit */
		if (bit_idx == 0) {
			bit_idx = 7;
			byte_idx++;
			data = (ws2812->led_data[byte_idx] * ws2812->brightness) >> 8;
		} else {
			bit_idx--;
		}
	}

	lpc_enable_irq();
}


/* Send led data from internal buffer (set leds to the selected color).
 * If nb_leds is 0 then all led data set using ws2812_set_pixel() since the last call
 *   to ws2812_clear_buffer(), ws2812_clear() or ws2812_stop() will be sent.
 * Call to this function will disable interrupts due to timming restrictions during
 *   the call to ws2812_bit_sender().
 */
int ws2812_send_frame(struct ws2812_conf* ws2812, uint16_t nb_leds)
{
	if (nb_leds > ws2812->nb_leds) {
		nb_leds = ws2812->nb_leds;
	}
	if (nb_leds == 0) {
		nb_leds = ws2812->max_led + 1;
	}
	ws2812->nb_bytes = nb_leds * 3;
	ws2812_bit_sender(ws2812);
	return 0;
}

void ws2812_set_brightness(struct ws2812_conf* ws2812, uint8_t b)
{
	ws2812->brightness = b + 1;
}

/* Set a pixel (led) color in the data buffer */
int ws2812_set_pixel(struct ws2812_conf* ws2812, uint16_t pixel_num, uint8_t red, uint8_t green, uint8_t blue)
{
	if (pixel_num >= ws2812->nb_leds) {
		return -1;
	}
	ws2812->led_data[ ((pixel_num * 3) + 0) ] = green;
	ws2812->led_data[ ((pixel_num * 3) + 1) ] = red;
	ws2812->led_data[ ((pixel_num * 3) + 2) ] = blue;
	if (ws2812->max_led < pixel_num) {
		ws2812->max_led = pixel_num;
	}
	return 0;
}
/* Get a pixel (led) color from the data buffer */
int ws2812_get_pixel(struct ws2812_conf* ws2812, uint16_t pixel_num, uint8_t* red, uint8_t* green, uint8_t* blue)
{
	if (pixel_num >= ws2812->nb_leds) {
		return -1;
	}
	*green = ws2812->led_data[ ((pixel_num * 3) + 0) ];
	*red = ws2812->led_data[ ((pixel_num * 3) + 1) ];
	*blue = ws2812->led_data[ ((pixel_num * 3) + 2) ];
	return 0;
}

/* Clear the internal data buffer. */
void ws2812_clear_buffer(struct ws2812_conf* ws2812)
{
	memset(ws2812->led_data, 0, (ws2812->nb_leds * 3));
	ws2812->max_led = 0;
}

/* Clear the internal data buffer and send it to the Leds, turning them all off */
void ws2812_clear(struct ws2812_conf* ws2812)
{
	/* Start at first led and send all leds off */
	ws2812_clear_buffer(ws2812);
	ws2812_send_frame(ws2812, ws2812->nb_leds);
	ws2812->max_led = 0;
}

void ws2812_stop(struct ws2812_conf* ws2812) __attribute__ ((alias ("ws2812_clear")));

